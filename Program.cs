﻿/*
 * Copyright Daisee, see LICENSE.
 */
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.IO;
using System.Net.Http.Headers;

namespace webserviceRequester
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!"); // verify that we get into the program
            var json = @"{
                ""direction"": ""outbound"",
                ""call_start"": ""2017-03-01T13:00:00.007Z"",
                ""length_of_whole_call"": 25.3,
                ""agent_reference"": ""mikaela.jones"",
                ""stereo_recording_agent_side"": ""left"",
                ""recordings"": [
                    {
                    ""timestamp_of_recording_start"": ""2017-03-01T13:00:27.845+10:00""
                    }
                ]
                }";
            var result = PostData(json, "file.mp3");
            if(!result.IsSuccessStatusCode){
                // print our error message
                Console.WriteLine(result.Content.ReadAsStringAsync().Result);
            }
            Console.WriteLine(result.ToString());
        }
        // const string url = "https://172.16.96.2/api/v1.0/upload";
        const string url = "https://upload.daiseelabs.com/api/v1.0/upload";
        // const string url = "http://localhost:6868/api/v1.0/upload";
        private static HttpResponseMessage PostData(string metadata, string filePath){
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer azyasdf234sa");
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            MultipartFormDataContent form = new MultipartFormDataContent();
            form.Add(new StringContent(metadata), "metadata");
            var file = File.ReadAllBytes(filePath);
            var someFileName = "d.mp3";
            form.Add(new ByteArrayContent(file), "mediafiles", someFileName);
            var response = httpClient.PostAsync(url, form).Result;
            httpClient.Dispose();
            return response;
        }
    }
}
