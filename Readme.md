# Usage
To compile, install `.net`, these are instructions for most distributions:
https://www.microsoft.com/net/learn/get-started/linux/fedora28

For quick changes install visual code:

* https://code.visualstudio.com/
* https://code.visualstudio.com/docs/languages/csharp

Although one could also setup their favorite editor with a plugin.

## Running

To run this program one requires an audio file called `file.mp3` in the root of
this project.
This can be any audio file, preferably with speech.

One may also consider replacing the inline JSON with type definitions.
 
## License

MIT
